Light helpers to facilitate `.mggd` creation.

Requires:
GIMP 2+ (and thus restricted to Python 2.7)

Structure your deck dir as follows:
  DECK_DIR
  |-`xcf` (gimp `.xcf` files)
  |-`png` (staged output for sampling)

Each gimp `.xcf` file should meet the following requirements:
  Have exactly 2 layers: a foreground and a background.
  The background must be a solid color, and will be automatically pulled in as a bg color of the MGGD deck tool.
  
To use these, fill out the details in `example_driver.txt`. It's a misnomer since it's not actually a driver, but you will copy and paste the lines in this file into the GIMP Python console.
There are 2 primary supported methods:
`main_gimp_export`: Exports GIMP projects in the `xcf` dir into the `png` dir.
`main_gimp_package`: Exports GIMP projects in the `xcf` dir into a `.mggd` file.

By default, both `main*` methods will use the file name as the character name.  There is a hook to specify a custom rename method.