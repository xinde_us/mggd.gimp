#!/usr/bin/env python

from __future__ import print_function
# python2 because gimp

import json
import os
import re
import shutil
import struct
import tempfile

# mafiagg_common

def is_png(data):
    return (data[:8] == '\211PNG\r\n\032\n'and (data[12:16] == 'IHDR'))


def get_png_dimensions(file_name):  # Tuple[int, int]
    with open(file_name, 'rb') as f:
        data = f.read(25)  # Header info only
    if is_png(data):
        w, h = struct.unpack('>LL', data[16:24])
        width = int(w)
        height = int(h)
    else:
        raise Exception('not a png image')
    return width, height


def get_image_file_names(dir_path=None, extension=None):  # Generator[str]
    if extension is None:
        extension = '.xcf'
    if not dir_path:
        dir_path = "esports"
    return (f for f in os.listdir(dir_path) if f.endswith(extension))


def gimp_get_images(dir_path=None):  # Generator[GimpImage]
    import gimpfu
    from gimpfu import pdb
    return (pdb.gimp_xcf_load(0, os.path.join(dir_path, xcf_name), xcf_name)
            for xcf_name in get_image_file_names(dir_name, extension='.xcf'))


class MggCharacter:

    def __init__(self, name, bg_color, image_file_path=None):
        self.name = name
        self.bg_color = bg_color
        if image_file_path:
            self.set_image(image_file_path)

    @property
    def id(self):  # int
        return int(self.image_file_name[4:])

    def set_image(self, image_file_path):
        self.width, self.height = get_png_dimensions(image_file_path)
        self.image_file_name = image_file_path.rpartition(os.path.sep)[-1]
        if self.image_file_name.endswith('.png'):
            raise Exception('incorrect format: ends with .png')
        elif not self.image_file_name.startswith('img-'):
            raise Exception('incorrect format: does not start with `img-`')

    def to_mggd_def(self):  # Dict[str, Any]
        return {
            "avatarCrop": {
                "height": self.height,
                "width": self.width,
                "x": 0,
                "y": 0,
            },
            "backgroundColor": self.bg_color,
            "id": self.id,
            "name": self.name,
        }


def color_percent_to_hex(color): # str
    color *= 256
    color = '%02x' % color
    color = color if color != '100' else 'ff'
    return color


def rgb_to_hex(red, green, blue):  # str
    red = color_percent_to_hex(red)
    green = color_percent_to_hex(green)
    blue = color_percent_to_hex(blue)
    return '#%s%s%s' % (red, green, blue)


def gimp_mgg_format_dir(dir_path, deck_name, version, output_file_path, rename_func=None, test=False):
    import gimpfu
    from gimpfu import pdb
    characters = []
    output_data = {
        'builtin': False,
        'characters': characters,
        'name': deck_name,
        'version': version,
    }
    tmp_dir = tempfile.mkdtemp()
    try:
        # Copy character files (and renaming if necessary)
        for i, xcf_name in enumerate(get_image_file_names(dir_path, extension='.xcf')):
            print(xcf_name)
            image = pdb.gimp_xcf_load(0, os.path.join(dir_path, xcf_name), xcf_name)
            layers = image.layers
            content_layer = layers[0]
            bg_layer = layers[1]
            # Get background color
            bg_color_obj = pdb.gimp_image_pick_color(image, bg_layer, 1, 1, gimpfu.FALSE, gimpfu.FALSE, 0)
            bg_color = rgb_to_hex(bg_color_obj.red, bg_color_obj.green, bg_color_obj.blue)
            # Export file for packaging
            pdb.gimp_item_set_visible(bg_layer, gimpfu.FALSE)
            pdb.gimp_image_merge_visible_layers(image, gimpfu.CLIP_TO_IMAGE)
            export_file_name =  'img-%(i)d' % {'i': i}
            export_file_path = os.path.join(tmp_dir, export_file_name)
            drawable = pdb.gimp_image_get_active_drawable(image)
            # pdb.gimp_file_save(image, drawable, os.path.join(dir, file_name), file_name)  # Saves by extension
            pdb.file_png_save_defaults(image, drawable, export_file_path, export_file_name)
            pdb.gimp_image_delete(image)
            if rename_func:
                name = rename_func(xcf_name)
                if name is None:
                    continue
            else:
                name = xcf_name[:-4]  # Truncate `.xcf`
            character = MggCharacter(name, bg_color, image_file_path=export_file_path)
            characters.append(character.to_mggd_def())
            if test:
                break
        # Write metadata file
        with open(os.path.join(tmp_dir, 'metadata.json'), 'w') as f:
            f.write(json.dumps(output_data))
        # Zip all the contents together
        shutil.make_archive(output_file_path, 'zip', tmp_dir)  # Appends `.zip`
        shutil.copy(output_file_path + '.zip', output_file_path)
    except Exception:
        shutil.rmtree(tmp_dir)
        raise


def gimp_export_all(base_dir):
    import gimpfu
    from gimpfu import pdb
    errors = 0
    for xcf_name in get_image_file_names(os.path.join(base_dir, 'xcf'), extension='.xcf'):
        print(xcf_name)
        image = pdb.gimp_xcf_load(0, os.path.join(base_dir, 'xcf', xcf_name), xcf_name)
        # Export file for packaging
        pdb.gimp_image_merge_visible_layers(image, gimpfu.CLIP_TO_IMAGE)
        export_file_name =  xcf_name.replace('xcf', 'png')
        export_file_path = os.path.join(base_dir, 'png', export_file_name)
        drawable = pdb.gimp_image_get_active_drawable(image)
        # pdb.gimp_file_save(image, drawable, os.path.join(dir, file_name), file_name)  # Saves by extension
        if image.width != image.height:
            print('WARNING: %(export_file_name)s is not square' % {'export_file_name': export_file_name})
            errors += 1
        pdb.file_png_save_defaults(image, drawable, export_file_path, export_file_name)
        pdb.gimp_image_delete(image)
    if errors:
        errors = int(errors)
        raise Exception('FAIL: Found %(errors) errors' % {'errors': errors})


def main_gimp_package(deck_name, version, test=False):
    this_dir = os.getcwd()
    base_dir = os.path.join(this_dir)
    out_dir = os.path.join(base_dir, 'png')
    staging_dir = os.path.join(base_dir, 'xcf')

    output_file_path = os.path.join(base_dir, '%(deck_name)s.mggd' % {'deck_name': deck_name})
    gimp_mgg_format_dir(staging_dir, deck_name, version, output_file_path, test=test)

def main_gimp_export():
    this_dir = os.getcwd()
    base_dir = os.path.join(this_dir)
    gimp_export_all(base_dir)

if __name__ == '__main__':
    main()
